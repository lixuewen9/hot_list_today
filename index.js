function querystring(obj) {
    let str = []

    for (let i in obj) {
        str.push(`${i}=${obj[i]}`)
    }

    return str.join('&')
}

function ajax(url, params) {
    const http = new XMLHttpRequest()

    return new Promise((resolve, reject) => {
        http.onreadystatechange = function() {
            if (http.readyState === 4) {
                if (http.status === 200) {
                    resolve(JSON.parse(http.responseText).data)
                } else {
                    reject(new Error('ajax error'))
                }
            }
        }

        let _url = params ? `${url}?${querystring(params)}` : url
        http.open('GET', _url, true)
        http.send()
    })
}

new Vue({
    el: "#app",
    data() {
        return {
            list: []
        }
    },
    created() {
        let type_list = ['52pojie', 'douyin', 'weibo', 'weixin', 'baidu', 'toutiao', 'hitory', 'juejin', 'bilibili']

        for (let i of type_list) {
            ajax('https://v1.alapi.cn/api/tophub/get', {
                type: i
            }).then(res => {
                this.list.push(res)
            }).catch(err => {
                console.log(err)
            })
        }


    }
})
